# io_scene_kcl

This is a Blender add-on to imports the KCL collision data of a Mario Kart 8 track.

![Blender screenshot](https://gitlab.com/Syroot/NintenTools/io_scene_kcl/raw/master/doc/readme/example.png)

S. the wiki for [help and more information](https://gitlab.com/Syroot/NintenTools/io_scene_kcl/wikis).

## Deprecation Notice

**This project aswell as other NintenTools projects is no longer updated or maintained.**
The following known issues result from this:
- The add-on cannot export functional collision models due to an unfixed octree creation bug (you will simply fall through the track in-game).
- The add-on does not work with Blender 2.80+.
